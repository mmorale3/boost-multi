#ifdef COMPILATION_INSTRUCTIONS
${CUDACXX:-nvcc} -std=c++17 -O3 $0 -o $0x  `#-x cu --extended-lambda --expt-relaxed-constexpr --Werror=cross-execution-space-call -Xcudafe=--display_error_number` -lboost_unit_test_framework -lboost_timer -D_DISABLE_CUDA_SLOW &&$0x&&rm $0x; exit
#endif

#define BOOST_TEST_MODULE "C++ Unit Tests for Multi CUDA pointers"
#define BOOST_TEST_DYN_LINK
#include<boost/test/unit_test.hpp>


#include "../../cuda/ptr.hpp"
#include "../../cuda/malloc.hpp"
#include "../../../../complex.hpp"

namespace multi = boost::multi;
namespace cuda = multi::memory::cuda;

namespace utf = boost::unit_test;

template<class T> __device__ void WHAT(T&&) = delete;

#if __CUDA_ARCH__
__device__ void f(cuda::ptr<double>){
//	printf("%f", *p);
//	printf("%f", static_cast<double const&>(*p));
}
#endif

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ptr){

//	static_assert( not std::is_convertible<std::complex<double>*, multi::memory::cuda::ptr<std::complex<double>>>{}, "!" );
//	static_assert( not std::is_convertible<multi::memory::cuda::ptr<std::complex<double>>, std::complex<double>*>{}, "!" );

	multi::memory::cuda::ptr<std::complex<double>> xxx = nullptr;
	std::complex<double>* ppp = raw_pointer_cast(xxx); (void)ppp;
	{
		auto ppp = static_cast<multi::memory::cuda::ptr<std::complex<double>>>(cuda::malloc(1*sizeof(std::complex<double>)));
		std::complex<double> const dd{*ppp};
//		assert( dd == std::complex<double>{0} );
	}
	using T = double; 
	static_assert( sizeof(cuda::ptr<T>) == sizeof(T*), "!");
	std::size_t const n = 100;
	{
		using cuda::ptr;
		auto p = static_cast<ptr<T>>(cuda::malloc(n*sizeof(T)));
CUDA_SLOW( 
		*p = 99.; 
)
		{
			ptr<T const> pc = p;
			BOOST_REQUIRE( *p == *pc );
		}
		BOOST_REQUIRE( CUDA_SLOW( *p == 99. ) );
		BOOST_REQUIRE( *p != 11. );
		cuda::free(p);
		
		cuda::ptr<T> P = nullptr; 
		BOOST_REQUIRE( P == nullptr );
		ptr<void> pv = p; (void)pv;
	}
//	what<typename cuda::ptr<T>::rebind<T const>>();
//	what<typename std::pointer_traits<cuda::ptr<T>>::rebind<T const>>();
	static_assert( std::is_same<typename std::pointer_traits<cuda::ptr<T>>::rebind<T const>, cuda::ptr<T const>>{} , "!");	
}

BOOST_AUTO_TEST_CASE(ptr_conversion){
	cuda::ptr<double> p = nullptr;
	cuda::ptr<double const> pc = p; (void)pc;
	static_assert(not std::is_convertible<cuda::ptr<double>, double*>{});
}

template<class T> struct Complex_{T real; T imag;};

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ptr_member_pointer){
	
	Complex_<double> c{10.,20.};
//	double Complex_<double>::* 
	Complex_<double>* p = &c;
	auto pm = &Complex_<double>::imag;
	BOOST_REQUIRE( p->*pm == 20. );
	BOOST_REQUIRE( *p.*pm == 20. );

//	cuda::ptr<Complex_<double>> pcu;
//	pcu->*pm;
}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_plus){

	using T = double;
	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = 99.; 
	
	BOOST_REQUIRE( *a + 1. == 100. );
	BOOST_REQUIRE( 1. + *a == 100. );

	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b = *a + 1.;
	BOOST_REQUIRE( *b == 100. );

	cuda::free(b);
	cuda::free(a);
}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_plus_complex){

	using T = std::complex<double>;
	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{99.}; 
	
	BOOST_REQUIRE( *a + T{1.} == T{100.} );
	BOOST_REQUIRE( T{1.} + *a == T{100.} );

	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b = *a + T{1.};
	BOOST_REQUIRE( *b == T{100.} );

	cuda::free(b);
	cuda::free(a);
}


BOOST_AUTO_TEST_CASE_TEMPLATE(multi_memory_cuda_ref_operator_minus, 
	T, decltype(std::tuple<
		double, std::complex<double>
	>{})
){

	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{99.}; 
	
	BOOST_REQUIRE( *a - T{1.} == T{+98.} );
	BOOST_REQUIRE( T{1.} - *a == T{-98.} );

	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b = *a - 1.;
	BOOST_REQUIRE( *b == 98. );

/*
	*b += 1.;
	BOOST_REQUIRE( *b == 99. );

	*b -= 1.;
	BOOST_REQUIRE( *b == 98. );

	*b *= 2.;
	BOOST_REQUIRE( *b == 196. );

	*b /= 2.;
	BOOST_REQUIRE( *b == 98. );
*/

	++(*b);
	BOOST_REQUIRE( *b == 99. );

	--(*b);
	BOOST_REQUIRE( *b == 98. );

	cuda::free(b);
	cuda::free(a);

}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_dummy){

        //using T = boost::multi::complex<double>;
        using T = std::complex<double>;
        auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
        *a = T(99.);

        BOOST_REQUIRE( *a - T(1.) == T(+98.) );
        BOOST_REQUIRE( T(1.) - *a == T(-98.) );

        auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));

        *b = *a - T(1.);
        BOOST_REQUIRE( *b == T(98.) );

        auto const c = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));

        *c = *a - *b;
        BOOST_REQUIRE( *c == T(1.) );
        BOOST_REQUIRE( *a - *b == T(1.) );
	
        *b += T(1.);
        BOOST_REQUIRE( *b == T(99.) );

        *b -= T(1.);
        BOOST_REQUIRE( *b == T(98.) );

        *b *= T(2.);
        BOOST_REQUIRE( *b == T(196.) );

        *b /= T(2.);
        BOOST_REQUIRE( *b == T(98.) );

        cuda::free(b);
        cuda::free(a);
        cuda::free(c);

}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_times){

	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{99.}; 
	
	BOOST_REQUIRE( *a * T{2.} == T{+198.} );
	BOOST_REQUIRE( T{2.} * *a == T{+198.} );

	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b = *a * T{2.};
	BOOST_REQUIRE( *b == T{198.} );

	cuda::free(b);
	cuda::free(a);

}

BOOST_AUTO_TEST_CASE_TEMPLATE(multi_memory_cuda_ref_operator_div,
	T, decltype(std::tuple<
		double, std::complex<double>
	>{})
){
	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{98.}; 
	
	BOOST_REQUIRE( *a / T{2.}   == T{+49.} );
	BOOST_REQUIRE( T{196.} / *a == T{+ 2.} );

	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b = *a / T{2.};
	BOOST_REQUIRE( *b == T{49.} );

	cuda::free(b);
	cuda::free(a);

}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_rel_equal){

	using T = double;
	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a =  99.; 
	
	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b =  99.;
	
	BOOST_REQUIRE( *a == *b );

	cuda::free(b);
	cuda::free(a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(multi_memory_cuda_ref_operator_plus_assign,
	T, decltype(std::tuple<
		double, std::complex<double>
	>{})
){
	auto a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{98.}; 
	*a += T{2.};

	BOOST_REQUIRE( *a == T{+100.} );

	cuda::free(a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(multi_memory_cuda_ref_operator_minus_assign,
	T, decltype(std::tuple<
		double, std::complex<double>
	>{})
){
	auto a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a = T{98.}; 
	*a -= T{2.};

	BOOST_REQUIRE( *a == T{+96.} );

	*a -= *a;
	BOOST_REQUIRE( *a == T{0.} );

	cuda::free(a);
}

BOOST_AUTO_TEST_CASE(multi_memory_cuda_ref_operator_rel_geq){

	using T = double;
	auto const a = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*a =  100.; 
	
	auto const b = static_cast<cuda::ptr<T>>(cuda::malloc(1*sizeof(T)));
	*b =  99.;
	
	BOOST_REQUIRE( *a > *b );
	BOOST_REQUIRE( *a >= *b );

	BOOST_REQUIRE( *b < *a );
	BOOST_REQUIRE( *b <= *a );

	cuda::free(b);
	cuda::free(a);
}

